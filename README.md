# big-data-cw1

To run the project, first run `pipenv install`, then run the `mongoDB.populate.py` script using:

```
pipenv run python main.py
```