from bson.objectid import ObjectId


class Image:
    """An Image class representing an Image document in MongoDB"""

    def __init__(self, filename: str, title: str, b64_encoded_string: str):
        self._id = ObjectId()
        self.filename = filename
        self.title = title
        self.b64_encoded_string = b64_encoded_string
