class Comment:
    """ A Comment class representing a Comment object """

    def __init__(self, timestamp, username, comment):
        self.timestamp = timestamp
        self.username = username
        self.comment = comment