from datetime import datetime
from .image import Image
from .comment import Comment
from .author import Author
from typing import List
from bson.objectid import ObjectId
from faker import Faker
from ..data.authors import authors
from ..data.images import images
import random
from pprint import pprint

faker = Faker()


def getRandomArrayId(x: list):
    shuffled_x = x.copy()
    shuffled_x_id = list(
        map(lambda x: x.__dict__['_id'], shuffled_x))
    random.shuffle(shuffled_x_id)

    random_number = faker.pyint(1, len(shuffled_x))
    # limit number of x, with high chance of 1
    number_of_x = random_number if random_number < 4 else 1

    return shuffled_x_id[0:number_of_x]


class Book:
    """A Book class representing a Book document in MongoDB"""

    def __init__(self):

        categories = ['MATH', 'FANTASY', 'SCIENCE',
                      'HORROR', 'CRIME', 'EDUCATION']

        comments = []

        # generate fake comments
        for _ in range(faker.pyint(0, 15)):
            comments.append(
                Comment(faker.date_time_this_year(), faker.simple_profile()[
                        'username'], faker.paragraph(nb_sentences=faker.pyint(1, 5))).__dict__
            )

        comments.sort(key=lambda comment: comment['timestamp'])

        self._id = ObjectId()
        self.title = faker.sentence(nb_words=faker.pyint(3, 8)).title()[:-1]
        self.authors = getRandomArrayId(authors)
        self.description = faker.paragraph(faker.pyint(2, 5))
        self.published = faker.date_time()
        self.category = categories[faker.pyint(0, len(categories) - 1)]
        self.pages = faker.pyint(200, 700)
        self.comments = comments
        self.images = getRandomArrayId(images)
        self.isbn = faker.isbn10()
