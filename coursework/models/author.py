from faker import Faker
from ..data.images import images
from bson.objectid import ObjectId

faker = Faker()


class Author:
    """ An Author class representing an Author document """

    def __init__(self):
        self._id = ObjectId()
        self.title = faker.prefix()
        self.name = faker.name()
        self.description = faker.paragraph(nb_sentences=faker.pyint(1, 5))
        self.image = None if faker.pyint(
            0, 10) < 2 else images[faker.pyint(0, len(images) - 1)]._id
