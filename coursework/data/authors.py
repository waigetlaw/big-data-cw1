from ..models.author import Author

# Generates 3 random authors
authors = list(map(lambda x: Author(), [None] * 12))
