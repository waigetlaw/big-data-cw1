from coursework.models.book import Book
from datetime import datetime
from .images import images

# Generates 30 random books
books = list(map(lambda x: Book(), [None] * 30))
