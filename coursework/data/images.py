import base64
from coursework.models.image import Image
from os import listdir
from os.path import isfile, join, dirname


def getBase64EncodedString(filename: str) -> str:
    with open(dirname(__file__) + "\\..\\resources\\" + filename, "rb") as image_file:
        return base64.b64encode(image_file.read())


def filenameToTitle(filename: str) -> str:
    text = filename.split('.')[0]
    text = text.replace('-', ' ')
    return text.title()


mypath = dirname(__file__) + "\\..\\resources"
imageFiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]

# This will create a list of image objects of all files found in the resources directory
images = list(map(lambda filename: Image(filename, filenameToTitle(
    filename), getBase64EncodedString(filename)), imageFiles))
