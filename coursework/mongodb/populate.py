from pymongo import MongoClient, ASCENDING
from coursework.data.books import books
from coursework.data.images import images
from coursework.data.authors import authors


def populate():
    client = MongoClient()

    client.drop_database("bookstore")

    db = client.bookstore
    books_collection = db.books
    books_collection.create_index("published")
    books_collection.create_index("title")
    books_collection.create_index(
        [("category", ASCENDING), ("published", ASCENDING)])

    images_collection = db.images
    authors_collection = db.authors
    authors_collection.create_index("name")

    images_collection.insert_many(map(lambda img: img.__dict__, images))
    books_collection.insert_many(map(lambda book: book.__dict__, books))
    authors_collection.insert_many(
        map(lambda author: author.__dict__, authors))
